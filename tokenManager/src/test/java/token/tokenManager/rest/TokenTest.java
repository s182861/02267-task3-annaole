package token.tokenManager.rest;

import static org.junit.Assert.*;

import java.util.List;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import token.tokenManager.rest.Customer;
import token.tokenManager.rest.Token;

public class TokenTest {

	Customer customer;
	String customerId;
	String customerId2;
	List<Token> tokens;
	String errorMessage;

	@Before
	public void setUp() throws Exception {
		TokenManager.getRepo().clearRepository();
		customerId = "123456-7890";
		customer = new Customer(customerId);
	}

	@Given("^that I am \"([^\"]*)\"$")
	public void thatIAm(String registerStatus) throws Throwable {
		if(registerStatus.equals("registered")){
			tokens = TokenManager.requestTokens(customerId,1);
			tokens = TokenManager.useToken(tokens.get(0).getString());
		}
	}
	
	@Given("^that I have (\\d+) tokens$")
	public void thatIHaveTokens(int arg1) throws Throwable {
		tokens = TokenManager.requestTokens(customerId,arg1);
	}

	@Given("^that I have (\\d+) used tokens$")
	public void thatIHaveUsedTokens(int arg1) throws Throwable {
		for (int i = 0; i < arg1; i++) {
			tokens = TokenManager.useToken(tokens.get(i).getString());
		}
	}
	
	@When("^I request (\\d+) token$")
	public void iRequestToken(int arg1) throws Throwable {
		try {
			tokens = TokenManager.requestTokens(customerId, arg1);
		} catch (Exception e) {
			errorMessage = e.getMessage();
		}
		
	}
	
	@When("^I request (\\d+) tokens$")
	public void iRequestTokens(int arg1) throws Throwable {
		try {
			tokens = TokenManager.requestTokens(customerId, arg1);
		} catch (Exception e) {
			errorMessage = e.getMessage();
		}
	}

	@When("^I use a token$")
	public void iUseToken() throws Throwable {
	    try {
	    	tokens = TokenManager.useToken(tokens.get(0).getString());
		} catch (Exception e) {
			errorMessage = e.getMessage();
		}
	}

	@When("^I use a fake token$")
	public void iUseAFakeToken() throws Throwable {
	    try {
	    	 tokens = TokenManager.useToken("fakeToken");
		} catch (Exception e) {
			errorMessage = e.getMessage();
		}
	}

	@Then("^I have (\\d+) unused token$")
	public void iHaveUnusedToken(int arg1) throws Throwable {
		int unusedTokens = 0;
		for (int i = 0; i < tokens.size(); i++) {
			if (!tokens.get(i).isUsed()) {
				unusedTokens++;
			}
		}
	    assertEquals(unusedTokens, arg1);
	}
	
	@Then("^I have (\\d+) token$")
	public void iHaveToken(int arg1) throws Throwable {
	    assertEquals(tokens.size(), arg1);
	}

	@Then("^I am registered$")
	public void iAmRegistered() throws Throwable {
		assertNotNull(TokenManager.getRepo().getCustomer(customerId));
	}

	@Then("^there is an error message \"(.*?)\"$")
	public void thereIsAnErrorMessage(String message) throws Throwable {
		assertEquals(message, errorMessage);
	}
	
}
