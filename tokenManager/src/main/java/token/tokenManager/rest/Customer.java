package token.tokenManager.rest;

import java.util.List;

public class Customer {
	private String uniqueId; // TODO: change type?
	private List<Token> myTokens;
	
	public Customer(String id) {
		uniqueId = id;
	}
	
	public List<Token> getTokens() {
		return myTokens;
	}
	public String getID() {
		return uniqueId;
	}
	public void updateTokens(List<Token> tokens) {
		myTokens = tokens;
	}
}
