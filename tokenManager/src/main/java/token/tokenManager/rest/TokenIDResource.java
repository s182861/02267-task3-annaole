package token.tokenManager.rest;


import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;

@Path("/tokens/{tokenid}")
public class TokenIDResource {
	
	// POST, because useToken is not idempotent
	@POST // TODO: is it ok without consumes?
	@Produces("text/plain")
	public Response useToken(@PathParam("tokenid") String tokenId) {
		try {
			
			//System.out.println("Repo customer token 0 id: " + DTUPayApplication.getRepo().getCustomerTokens(customerId).get(0).getString());
			//System.out.println("Received customerid: "+ customerId);
			//System.out.println("Received tokenid: " + tokenId);
			TokenManager.useToken(tokenId);
			return Response.ok("Token used").build();
			
		} catch (Exception e){
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}
}

