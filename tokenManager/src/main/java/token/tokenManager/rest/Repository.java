package token.tokenManager.rest;
import java.util.List;

/*
 * Interface for dependency inversion 
 */

public interface Repository {

	public Customer getCustomer(String id);

	public void clearRepository(); 

	public void addCustomer(Customer customer);

	public void removeCustomer(Customer customer);

	public void updateCustomer(Customer customer, List<Token> newTokens);

	public List<Token> getCustomerTokens(String customerId);
	
	public List<Customer> getAllCustomers();
}
