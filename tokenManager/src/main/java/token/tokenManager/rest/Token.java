package token.tokenManager.rest;

public class Token {
	private String uniqueString;
	private boolean used;
	
	public Token(String uniqueString) {
		this.uniqueString = uniqueString;
	}
	
	public String getString() {
		return this.uniqueString;
	}
	
	public boolean isUsed() {
		return used;
	}
	
	public void use() {
		used = true;
	}
}
