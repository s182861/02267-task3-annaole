#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: Calculator
  Optional description of the feature
  
	Scenario: no numbers
		Given input is ""
    When calculating sum
    Then should return 0
    
   Scenario: one number
		Given input is "1"
    When calculating sum
    Then should return 1
    
   Scenario: two numbers
		Given input is "1,2"
    When calculating sum
    Then should return 3
    
   Scenario: any number
		Given input is "1,2,9"
    When calculating sum
    Then should return 12
    
   Scenario: new line
		Given input is "1\n2"
    When calculating sum
    Then should return 3
    
   Scenario: different delimiters
		Given input is "//;\n1;2;3;4"
    When calculating sum
    Then should return 10
    
   Scenario: negatives
		Given input is "//;\n1;-2;3;-4"
    When calculating sum
    Then should display error "Negatives not allowed: -2-4"