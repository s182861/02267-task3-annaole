import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.*;

public class StepDefinitions {
	private String input;
	private int result;
	private StringCalculator stringCalc;
	private String errorMessage;

	@Given("^input is \"(.*?)\"$")
	public void input_is(String str) throws Throwable {
		stringCalc = new StringCalculator();
        input = str;
	}
	
	@When("^calculating sum$")
    public void calculating_sum() throws Throwable {
		
        try {
			result = stringCalc.Add(input);
		} catch (NegativeNumbersException e) {
			errorMessage = e.getMessage();
		}
    }

	@Then("^should return (\\d+)$")
	public void should_return(int realSum) {
	    assertEquals(realSum, result);
	}
	
	@Then("^should display error \"(.*?)\"$")
	public void should_display_error(String message) {
	    assertEquals(message, errorMessage);
	}
}
