package Task3.Task3;

public class NegativeNumbersException extends Exception {
	public NegativeNumbersException(String errorMessage) {
		super(errorMessage);
	}
}