package Task3.Task3;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringCalculatorTest {
	
	@Test
	public void testNoNumber() throws NegativeNumbersException {
		
		StringCalculator stringCalc = new StringCalculator();
		
		assertEquals(stringCalc.Add(""), 0);
		
	}

	@Test
	public void testOneNumber() throws NegativeNumbersException {
		
		StringCalculator stringCalc = new StringCalculator();
		
		assertEquals(stringCalc.Add("5"), 5);
		
	}
	
	@Test
	public void testTwoNumbers() throws NegativeNumbersException {
		
		StringCalculator stringCalc = new StringCalculator();
		
		assertEquals(stringCalc.Add("5,2"), 7);
		
	}
	
	@Test 
	public void testAnyNumbers() throws NegativeNumbersException {
		
		StringCalculator stringCalc = new StringCalculator();
		
		assertEquals(stringCalc.Add("5,2,6"), 13);
		
	}
	
	@Test
	public void testNewLine() throws NegativeNumbersException {
		
		StringCalculator stringCalc = new StringCalculator();
		
		assertEquals(stringCalc.Add("5\n2,6"), 13);
		
	}
	
	@Test
	public void testDifferentDelimiters() throws NegativeNumbersException {
		
		StringCalculator stringCalc = new StringCalculator();
		
		assertEquals(stringCalc.Add("//;\n1;2\n3;4"), 10);
		
	}
	
	@Test
	public void testNegatives() {
		
		StringCalculator stringCalc = new StringCalculator();
		
		try {
			stringCalc.Add("//;\n1;-2\n3;-41");
		} catch (NegativeNumbersException e) {
			assertEquals(e.getMessage(),"Negatives not allowed: -2-41");
		}
		
	}

}
