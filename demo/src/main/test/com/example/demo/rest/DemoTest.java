package com.example.demo.rest;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.*;

import org.junit.Test;

/*
 * How to run:
 * a) mvn thorntail:run
 * b.1) mvn package
 * b.2) java -jar demo-thorntail.jar (in mvn)
 */

public class DemoTest {
	Client client = ClientBuilder.newClient();
	WebTarget r = client.target("http://02267-istanbul.compute.dtu.dk:8080/hello");
	
	@Test
	public void testGetDemoName() {
		String result = r.request().get(String.class);
		assertEquals("Hello from Thorntail!",result);
		System.out.println(result);
	}
}
