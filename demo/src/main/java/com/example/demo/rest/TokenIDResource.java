package com.example.demo.rest;


import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/tokens/{tokenid}")
public class TokenIDResource {
	
	@POST
	public void useToken(String customerid, @PathParam("tokenid") String tokenid) {
		try {
			DTUPayApplication.useToken(customerid, tokenid);
			//TokensResource.tokenManager.useToken(new Token(tokenid));
		} catch (Exception e){
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}
}

