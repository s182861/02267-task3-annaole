package com.example.demo.rest;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class InMemoryRepository {
	List<Token> tokens = new ArrayList<Token>();
	List<Customer> customers = new ArrayList<Customer>();
	
	// Check if customer ID already exists in DB.
	// Used when trying to register a new customer.
	public Customer getCustomer(String id){
		for (int i =0; i<customers.size(); i++){
			if (customers.get(i).getID().equals(id)){
				System.out.println(customers.get(i).getID());
				return customers.get(i);
			}
		}
		//TODO Throw error
		return null;
	}
	
	public void clearRepository(){
		tokens.clear();
		customers.clear();
	}
	public void addCustomer(Customer customer) {
		customers.add(customer);
	}
	
	public void removeCustomer(Customer customer) {
		customers.remove(customer);
	}
	
	/*public Token updateToken(Token token) {
		tokens.remove(token);
		token.use();
		tokens.add(token);
		
		return token;
	}*/

	public void updateCustomer(Customer customer, List<Token> newTokens) {
		customers.remove(customer);
		customer.updateTokens(newTokens);
		customers.add(customer);
	}
}
